import os
os.system("cls")

def menu():
    band=1
    print("1) ingreso de Productos")
    print("2) Mostrar el listado de Productos")
    print("3) Mostrar el producto segun la cantidad de Stock")
    print("4) aumentar X valor a todos los productos cuyo Stock es menor a Y")
    print("5)Eliminar todos los productos cuyo Stock sea igual a 0")
    print("6) Salir")
    opcion=0

    while band ==1:
        opcion=int(input("ingrese la opcion: "))
        if opcion <0 or opcion >6:
            print("el numero ingresado es incorrecto, por favor ingrese nuevamente")
            band=1
        else:
            band=0
    return opcion


def ingreso():
    lista=[]
    stock=-1
    precio=-1
    
    while stock<0 or precio <0:
        codigo=int(input("ingrese el CODIGO del producto (XXXX) : "))
        descricion=input("ingrese la DESCRIPCION del producto: ")
        precio=float(input("ingrese el PRECIO del producto: "))
        stock=int(input("ingrese el STOCK de productos: "))
        if stock<0 or precio <0:
            print("los valores ingresados son incorrectos, por favor ingrese nuevamente")

    lista=[descricion,precio,stock]
    return codigo, lista

def validacion(codigo,productos):
    band=0
    
    for keys in productos:
        if keys==codigo:
            band=1
            print("el codigo ingresado ya existe, por favor ingrese nuevamente")
    return band


def mostrarIntervalo(productos):
    lista=[]
    stockI=1
    stockF=0    
    while stockI>stockF:
        stockI=int(input("ingrese el stock Inicial:"))
        stockF=int(input("ingrese el stock Final:"))
        if stockI>stockF:
            print("los stocks ingresados son incorrectos, por favor ingrese nuevamente")

    for keys in productos:
        lista=productos[keys]
        if lista[2]>=stockI and lista[2]<=stockF:
            print("codigo: ",keys," producto: ",productos[keys])
    return input("ingrese una tecla para continuar...")

def aumenta(productos):
    y=int(input("ingrese el valor del stock que desea aumentan (Y):"))
    x=int(input("ingrese la cantidad de stock que desea aumentar (X):"))
    lista=[]    

    for keys in productos:
        lista=productos[keys]

        if lista[2]<=y:
            print("codigo: ",keys," producto: ",productos[keys])
            lista[2]=lista[2]+x
            productos[keys]=lista
            print("==> codigo: ",keys," producto: ",productos[keys])
    return productos

def elimina (productos):
    aux=[]
    for keys in productos:
        lista=productos[keys]

        if lista[2]==0:
            aux.append(keys)

    for x in range (len(aux)):
        print("codigo: ",aux[x]," producto: ",productos[aux[x]])
        del productos[aux[x]]
        print("-REMOVED-")

    return productos


#PPAL
productos={}
productos=dict()
resp="s"
opcion =0


while resp=="s":
    os.system("cls")    
    band =1
    opcion=menu()

    if opcion ==1:
        while band==1:
            cod,lista=ingreso()
            band=validacion(cod,productos)
        productos[cod]=lista
    elif opcion ==2:
        print("codigo:   XXXX   producto :[ Descripcion , Precio , Stock ]")
        for key in productos:
            print("codigo: ",key," producto: ",productos[key])
        input("pulse una tecla para continuar...")
    elif opcion==3:
        mostrarIntervalo(productos)
    elif opcion==4:
        productos=aumenta(productos)
        input("pulse una tecla para continuar...")
    elif opcion==5:
            productos=elimina(productos)
            input("pulse una tecla para continuar...")
    elif opcion==6:
        resp="n"
