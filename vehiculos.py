import os
from operator import itemgetter
def addvehiculos(vehiculos):
    vehiculos.append(["IBH123","Renault","Automóvil",2010,250000,600000,660000,"D"])
    vehiculos.append(["JBC321","Ford","Automóvil",2011,210000,590000,640000,"D"])
    vehiculos.append(["MKL850","Citroen","Automóvil",2013,150000,800000,880000,"V"])
    vehiculos.append(["AC123CA","Ford","Utilitario",2015,80000,850000,935000,"R"])
    vehiculos.append(["AB526PA","Fiat","Automóvil",2016,75000,900000,990000,"D"])
    vehiculos.append(["BD142CB","Renault","Utilitario",2017,55000,1100000,1210000,"R"])
    return vehiculos

def ingresoMatricula():
    matricula="000000000"
    while (len(matricula)>=8 or len(matricula)<=5):
        matricula=input("ingrese la matricula del vehiculo: ")
        if len(matricula)>=8 or len(matricula)<=5: 
            print("la matricula ingresada es invalida, por favor ingrese nuevamente")
    matricula=matricula.upper()
    return matricula

def ingresoMarca():
    marca="0"
    while marca!="R" and marca!="F" and marca !="C":
        print("ingrese la marca del vehiculo")
        marca=input("'R'- Renault// 'F'- Ford // 'C'- Citroen: ")
        marca=marca.upper()
        print(marca)
        if (marca!="R" and marca!="F" and marca !="C"):
            print("la marca ingresada es incorrecta")

    if marca=="R":
        marca="Renault"
    elif marca=="F":
        marca="Ford"
    else:
        marca="Citroen"
        
    return marca

def ingresoTipo():
    tipo="0"
    while tipo != "A" and tipo!="U":
        print("ingrese el tipo de vehiculo")
        tipo=input("'A'-Automovil// 'U'-Utilitario: ")
        tipo=tipo.upper()
        if tipo != "A" and tipo!="U":
            print("el tipo ingresado es incorrecto, por favor ingrese nuevamente")
    if tipo=="A":
        tipo="Automovil"
    else:
        tipo="Utilitario"

    return tipo

def ingresoModelo():
    modelo=0
    while modelo<2005 or modelo>2020:
        modelo=int(input("ingrese el modelo del vehiculo"))
        if modelo<2005 or modelo>2020:
            print("el modelo ingresado no es valido, por favor Ingrese nuevamente")
    return modelo

def agregar():
    lista=[]
    lista=list()
    matricula=ingresoMatricula()
    marca=ingresoMarca()
    tipo=ingresoTipo()
    modelo=ingresoModelo()
    kilometraje=input("ingrese el kilometraje del vehiculo")
    precio=float(input("ingrese el precio del vehiculo"))
    precioVenta=precio* 1.1
    estado="D"
    lista=[matricula,marca,tipo,modelo,kilometraje,precio,precioVenta,estado]
    return lista

def menu():
    os.system("cls")
    opc=0
    while opc<1 or opc>5:
        print("1) Agregar un Vehiculo")
        print("2) Buscar Automovil por su dominio")
        print("3) Ordenar la lista de automoviles segun su Marca")
        print("4) Ordenar la lista de automoviles segun su Precio")
        print("5) Salir")
        try:
            opc=int(input("ingrese un valor: "))
        except:
            opc=0 
        if opc<1 or opc>5:
                print("el valor ingresado es invalido, por favor ingrese nuevamente")
                input("presione una tecla para continuar...")
    return opc

def buscarDominio(dominio,vehiculos):
    aux =[]
    aux=list()
    print("el vehiculo encontrado es el siguiente: ")
    for i in range(len(vehiculos)):
        aux=vehiculos[i]
        if aux[0]==dominio:
            print(aux)
    return input("ingrese una tecla para continuar...")

def ordenar(vehiculos, orden, valor):   #orden:True=Desc o False=Ascendente,  valor: segun 1=marca o 6=precio
    lista=sorted(vehiculos, key=itemgetter(int(valor)),reverse=orden)
    for i in lista:
        print(i)
    return input("presione una tecla para continuar....")

#PPAL
vehiculos=[]
vehiculos=list()
vehiculos=addvehiculos(vehiculos)
res="s"
while res=="s":
    os.system("cls")

    opc=menu()

    if opc==1:
        vehiculos.append(agregar())
    elif opc==2:
        matricula=ingresoMatricula()
        buscarDominio(matricula,vehiculos)
    elif opc==3:
        orden=0
        print("Ordenar por Marca")
        while(orden<=0 or orden>=3):
            orden=int(input("ingrese 1-Ascendente, 2- Descendente"))
            if (orden<=0 or orden>=3):
                print("el valor ingresado es incorrecto, por favor ingrese nuevamente")
        if orden==1:
            orden=True
        elif orden==2:
            orden=False
        ordenar(vehiculos,orden,1)
    elif opc==4:
        orden=0
        print("Ordenar por Precio de Venta")
        while(orden<=0 or orden>=3):
            orden=int(input("ingrese 1-Ascendente, 2- Descendente"))
            if (orden<=0 or orden>=3):
                print("el valor ingresado es incorrecto, por favor ingrese nuevamente")
        if orden==1:
            orden=False
        elif orden==2:
            orden=True
        ordenar(vehiculos,orden,6)
    elif opc==5:
        res="n"